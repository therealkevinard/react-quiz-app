import React, {Component} from 'react';
import "./QuizQuestion.css";
import QuizAnswer from "./QuizAnswer";

class QuizQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: {}
        };
    }

    onSelection = (selected) => {
        this.setState({
            selected: selected
        });
        this.props.onSelection(this, selected);
    }

    render() {
        return (
            <div className={[
                'question-single-outer',
                `question-single-outer-question-number-${this.props.number}`,
                this.props.className
            ].join(' ')}>
                <div className="question-single-banner col-12">
                    <span className="question-single-number">{this.props.number}.</span> <span className="question-single-question">{this.props.text}</span>
                </div>
                {this.props.answers.map((ans, i) => {
                    return (
                        <QuizAnswer
                            className="col-sm-12 col-md-6 col-xl-3"
                            key={`question-number-${this.props.number}-answer-${i}`}
                            onClick={(selectedAnswer) => {
                                this.onSelection(selectedAnswer);
                            }}
                            selected={this.state.selected === ans}
                            answer={ans}
                            forbear={ans.forBear}
                            text={ans.text}/>
                    )
                })}
            </div>
        )
    }
}

export default QuizQuestion;