import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
// import logo from './logo.svg';
import './App.css';

import QuizQuestion from './components/QuizQuestion';

const QuizModel = require('./model/QuizModel');
const BearsModel = require('./model/BearsModel');

const initialState = {
    isSubmitted: false,
    calculated: [],
    shareString: '',
};

class App extends Component {
    constructor(props) {
        super(props);
        this.questions = QuizModel;
        this.bears = BearsModel;
        this.selections = {};
        this.state = initialState;
    }

    onAnswerQuestion(questionCmp, selection) {
        this.selections[questionCmp.props.number] = selection;
    }

    format() {
    }

    reset() {
        this.selections = {};
        this.setState(initialState);
        window.scrollTo(0, 0);
    }

    calculate() {
        let bearTallies = {};
        let sum = 0;
        let calculated = [];
        Object.keys(this.selections)
            .forEach(key => {
                let curr = this.selections[key];
                bearTallies[curr.forBear] ? bearTallies[curr.forBear]++ : bearTallies[curr.forBear] = 1;
                sum++;
            });
        Object.keys(bearTallies)
            .forEach(bear => {
                let calc = {
                    bear: bear,
                    percent: Math.floor((bearTallies[bear] / sum) * 100)
                };
                calc.bearInfo = this.bears.filter(bearInfo => {
                    return bearInfo.bear === bear;
                })[0];
                calculated.push(calc);
            });
        calculated.sort((a, b) => {
            return b.percent - a.percent;
        });

        this.setState({
            isSubmitted: true,
            calculated: calculated,
            shareString: `I'm ${calculated[0].percent}% ${calculated[0].bear}`
        });

        window.scrollTo(0, 0);
    }

    __createMarkup(html) {
        return {
            __html: html
        }
    }

    render() {
        return (
            <div className="App container-fluid">
                {!this.state.isSubmitted &&
                <div>
                    {this.questions.map((q, i) => {
                        return (
                            <QuizQuestion key={`question-${i}`}
                                          onSelection={(question, answer) => {
                                              this.onAnswerQuestion(question, answer)
                                          }}
                                          className={"row"}
                                          number={q.num}
                                          text={q.text}
                                          answers={q.answers}/>
                        )
                    })}
                    <div className="row justify-content-around button-step-row">
                        <button
                            onClick={() => {
                                this.calculate()
                            }}
                            className="col-8 btn btn-outline-dark">Calculate
                        </button>
                    </div>
                </div>
                }

                {this.state.isSubmitted &&
                <div>
                    <div className="row justify-content-end button-step-row">
                        <a className="btn btn-outline-dark" href={`https://www.facebook.com/dialog/share?app_id=370967823412374&display=page&redirect_uri=https://dancingbearlodge.com/&href=https://dancingbearlodge.com/what-kind-of-bear-are-you/&quote=${this.state.shareString}`}>Share your Results <i className="fa fa-facebook"></i></a>
                    </div>
                    <div className="row results-row">
                        {this.state.calculated.map((calc) => {
                            return (
                                <div
                                    key={Math.floor(Math.random() * 1000000)}
                                    className={[
                                        "calculation col-12 bear-single",
                                        `bear-${calc.bear.toLowerCase().replace(' ', '-')}`
                                    ].join(' ')}>
                                    <div className="row">
                                        <div className="bear-single-bear col-sm-12 col-lg-4">&nbsp;</div>
                                        <div className="bear-single-content col-sm-12 col-lg-8">
                                            <span className="percent">{calc.percent}%</span> <br/>
                                            <span className="bearname">{calc.bear}</span> <br/>
                                            <div className="bearinfo" dangerouslySetInnerHTML={this.__createMarkup(calc.bearInfo.desc)}></div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    <div className="row justify-content-around button-step-row">
                        <button
                            onClick={() => {
                                this.reset();
                            }}
                            className="col-8 btn btn-outline-dark">Start Over
                        </button>
                    </div>
                </div>
                }

            </div>
        );
    }
}

export default App;
